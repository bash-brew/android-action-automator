# Android Action Automator

## About

This Python script allows you to record and replay touch events on multiple Android devices simultaneously over standard ADB shell input commands.

## Features

- **Record:** Record touch events from an Android device and save them to a file.
- **Replay:** Replay recorded touch events on connected Android devices.
- **Network Discovery:** Discover Android devices on the network (Currently not functional).
- **Emulator Scaling:** Scale touch event coordinates for emulated Android devices.

## Usage

### Recording Touch Events

To record touch events from an Android device:
```bash
python3 main.py -f output_file.txt record
```

- Use `-f` or `--file` to specify the file to save recorded touch events.

### Replaying Touch Events

To replay recorded touch events on Android devices:
```bash
python3 main.py -f input_file.txt replay
```

- Use `-f` or `--file` to specify the file containing recorded touch events.

### Network Discovery

**Note:** Network device discovery functionality is currently not operational and will not discover devices on the network.

## Installation

1. Clone the repository:
```bash
git clone https://gitlab.com/bash-brew/android-action-automator.git
cd android-action-automator
```

2. Run The Script:
```bash
python3 main.py -f <file> <record/replay>
```

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please follow these steps:

1. Fork the repository and create a new branch.
2. Make your changes and test them thoroughly.
3. Submit a pull request with a clear description of your changes.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
