import argparse
import sys
import subprocess
import re
import threading
import time

def main():
    """
    Main function to parse arguments and run functions.
    """
    parser = argparse.ArgumentParser(description="Record and play touch events on Android devices.")
    parser.add_argument("action", choices=["record", "replay", "repeat"], help="Action to perform: record, replay, or repeat")
    parser.add_argument("-f", "--file", help="File to save recorded touch events or play from")
    parser.add_argument("-n", "--network", help="Record or play touch events on network devices using the specified subnet", default=None)
    parser.add_argument("-e", "--emu", help="Emulated device scale factor. Format: xx:yy. If unsure, use 33:73 and increment/decrement x/y until max matches resolution", default=None)
    args = parser.parse_args()

    if not check_dependency(args.network):
        return

    # Set default screen_scale value if not provided
    screen_scale = args.emu if args.emu else "1000:1000"

    if args.action == "record":
        perform_record_action(args.file, args.network)
    elif args.action == "replay":
        perform_replay_action(args.file, args.network, screen_scale)

def perform_record_action(file_path, network_subnet):
    """
    Perform the 'record' action: record touch events on Android device.
    """
    if not file_path:
        print("Please specify a file to save recorded touch events using the -f option.")
        return
    devices = get_devices(network_subnet)
    if devices:
        authorize_devices(devices)
        record_touchscreen_events(file_path, devices[0])  # Record events from the first device found

def perform_replay_action(file_path, network_subnet, screen_scale):
    """
    Perform the 'replay' action: replay recorded touch events on Android device(s).
    """
    if not file_path:
        print("Please specify a file to play recorded touch events from using the -f option.")
        return

    devices = get_devices(network_subnet)
    if devices:
        authorize_devices(devices)  # Authorize devices before replaying touch events
        replay_touchscreen_events(file_path, devices, screen_scale)

def is_command_available(command):
    """
    Check if a command is available in the system's PATH.
    """
    result = subprocess.run(["which", command], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return result.returncode == 0

def check_dependency(network_flag):
    """
    Check for dependencies (ADB and Nmap if network flag is used).
    """
    if not is_command_available("adb"):
        print("Error: ADB command is not available. Please make sure ADB is installed and accessible in your system PATH.")
        return False

    if network_flag and not is_command_available("nmap"):
        print("Error: Nmap command is not available. Please make sure Nmap is installed and accessible in your system PATH if you intend to use network discovery.")
        return False

    return True

def check_device_state(device):
    """
    Check the state of the device.
    """
    while True:
        device_state_output = subprocess.run(["adb", "-s", device, "get-state"], capture_output=True, text=True)
        device_state = device_state_output.stdout.strip()
        if device_state == "device":
            return device_state
        else:
            print(f"Device {device} is in state: {device_state}. Waiting for device to be in 'device' state...")
            time.sleep(2)  # Add a delay before rechecking

def authorize_device(device):
    """
    Authorize a single device by prompting the user to tap 'Accept'.
    """
    print(f"Please tap 'Accept' on device {device} and press Enter to continue...")
    input()  # Wait for user to press Enter
    time.sleep(.5)  # Add a small delay before rechecking

def authorize_devices(devices):
    """
    Authorize connected devices by checking their state.
    """
    print("Authorizing connected devices...")
    for device in devices:
        print(f"Checking device state for {device}...")
        while True:
            device_state = check_device_state(device)
            if device_state != "device":
                print(f"Error: Device {device} is not in 'device' state. Please ensure the device is connected and authorized.")
                authorize_device(device)  # Prompt user to authorize the device again
            else:
                print(f"Device {device} is connected and ready for use.")
                break

def get_devices(network_subnet):
    """
    Get a list of connected USB devices or discover network devices.
    """
    devices = get_usb_devices()
    if network_subnet:
        devices = discover_devices(network_subnet)
    if not devices:
        print("Error: No devices found.")
    return devices

def discover_devices(subnet=None):
    """
    Discover connected Android devices using nmap.
    """
    if subnet:
        print("Discovering devices on the network...")
        ip_pattern = re.compile(r'Nmap scan report for ([\d.]+)')
        adb_devices = []

        # Run nmap scan
        nmap_output = subprocess.check_output(["nmap", "-p", "5555", subnet]).decode("utf-8")

        # Extract IP addresses of devices running ADB
        for line in nmap_output.split('\n'):
            match = ip_pattern.match(line)
            if match:
                ip_address = match.group(1)
                adb_devices.append(ip_address)

        return adb_devices
    else:
        print("Error: Please specify a subnet for device discovery.")
        return []

def get_usb_devices():
    """
    Get a list of connected USB devices.
    """
    print("Finding USB-connected devices...")
    devices = subprocess.check_output(["adb", "devices"]).decode("utf-8").split("\n")[1:-2]
    usb_devices = [device.split()[0] for device in devices if "device" in device]
    return usb_devices

def record_touchscreen_events(output_file, device):
    """
    Record touchscreen events into output file.
    """
    print(f"Recording touch events from device {device} Press Ctrl+C to stop...")
    with open(output_file, 'w') as f:
        process = subprocess.Popen(['adb', '-s', device, 'shell', 'getevent', '-lt'], stdout=subprocess.PIPE)
        try:
            for line in iter(process.stdout.readline, b''):
                f.write(line.decode())
        except KeyboardInterrupt:
            print("\nRecording stopped.")
            print(f"Touch events recorded and saved to {output_file}")
            process.kill()

def replay_touchscreen_events(input_file, devices, screen_scale):
    """
    Replay recorded touch events on Android device(s).
    """
    print("Replaying recorded touch events...")
    events = parse_events(input_file, screen_scale)
    threads = []
    for device in devices:
        t = threading.Thread(target=exec_events, args=(device, events))
        threads.append(t)
        t.start()
    for t in threads:
        t.join()

def exec_events(device, events):
    """
    Execute adb shell input tap/swipe commands on device.
    """
    for event in events:
        adb_command = f"adb -s {device} shell {event}"
        print(f"Executing command on {device}: {event}")
        result = subprocess.run(adb_command, shell=True)
        if result.returncode != 0:
            print(f"Error executing command on {device}: {event}")
            sys.exit(1)

def parse_events(file_path, screen_scale):
    """
    Parse recorded touch events into adb shell input tap/swipe commands.
    """
    events = []  # List to store generated adb shell commands
    x_coords = []  # List to store x coordinates of touch events
    y_coords = []  # List to store y coordinates of touch events
    start_time = None  # Variable to store start time of a touch event
    previous_timestamp = None  # Variable to store the timestamp of the previous event

    # Handle emulated device scaling
    scale_parts = []    # List to store emulator screen scaling
    scale_parts = screen_scale.split(":")
    if len(scale_parts) != 2:
        raise ValueError("Invalid format for emulated device scale")
    x_scale, y_scale = scale_parts[0].strip(), scale_parts[1].strip()
    try:
        x_scale = float(x_scale) / 1000
        y_scale = float(y_scale) / 1000
    except ValueError:
        raise ValueError("Invalid value for emulated device scale")

    with open(file_path, 'r') as file:
        for line in file:
            components = line.strip().split()
            if len(components) < 5:  # Skip lines with too few components
                continue
            timestamp_str = components[1].rstrip(']')  # Strip trailing ']' from timestamp
            timestamp = float(timestamp_str)  # Convert timestamp to float

            if components[4] == 'ABS_MT_TRACKING_ID':

                if components[5] != 'ffffffff':  # New touch event: Store start time, Calculate sleep durration, Store prev x/y, Reset x/y arrays
                    start_time = timestamp  # Store the start time of the new event
                    if previous_timestamp is not None:
                        duration = (timestamp - previous_timestamp) * 1000  # Calculate sleep duration
                        events.append(f'sleep {(duration / 1000)}')  # Append sleep event.
                    if x_coords:
                        previous_x = x_coords[-1]    # Store previous x coordinate
                        x_coords = []               # Clear x array
                    if y_coords:
                        previous_y = y_coords[-1]    # Store previous y coordinate
                        y_coords = []               # Clear y array

                elif components[5] == 'ffffffff':  # Previous event ended: Calculate durration of event, Construct input commands
                    duration = (timestamp - start_time) * 1000  # Calculate duration of the touch event
                    if len(x_coords) == 1 and len(y_coords) == 1:  # Construct a tap at the specified location
                        events.append(f'input swipe {x_coords[0]} {y_coords[0]} {x_coords[0]} {y_coords[0]} {int(duration)}')
                    elif not x_coords and not y_coords:  # Construct a tap at the previous location
                        events.append(f'input swipe {previous_x} {previous_y} {previous_x} {previous_y} {int(duration)}')
                    elif not x_coords and y_coords: # Construct a tap at the old x location
                        events.append(f'input swipe {previous_x} {y_coords[-1]} {previous_x} {y_coords[-1]} {int(duration)}')
                    elif not y_coords and x_coords: # Construct a tap at the old y location
                        events.append(f'input swipe {x_coords[-1]} {previous_y} {x_coords[-1]} {previous_y} {int(duration)}')
                    elif x_coords and y_coords and (len(x_coords) > 1 or len(y_coords) > 1):  # Construct a slightly slower swipe
                        events.append(f'input swipe {x_coords[0]} {y_coords[0]} {x_coords[-1]} {y_coords[-1]} {int(duration) + 30}')

            elif components[4] == 'ABS_MT_POSITION_X':  # New x position
                x_coords.append(round(int(components[5], 16) * x_scale))  # Get x position
            elif components[4] == 'ABS_MT_POSITION_Y':  # New y position
                y_coords.append(round(int(components[5], 16) * y_scale))  # Get y position

            previous_timestamp = timestamp  # Store timestamp

    return events

if __name__ == "__main__":
    main()
